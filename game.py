from random import randint

# Propmt the person for their name

name = input("Hi! What is your name? ")

#Guess #1 <<guess number>> : <<name>> were you boren in <<m/yyyy>>?

for guess_number in range(1,6):
    month_number = randint(1, 12)
    year_number = randint(1924, 2004)

    print("Guess", guess_number, "were you born in", month_number, "/", year_number, "?")

    response = input("yes or no? ")

    if response == "yes":
        print("I knew it!")
        exit()
    elif guess_number ==5:
        print("I have other things to do, Good Bye.")
    else:
        print("Drat! Lemme try again")
